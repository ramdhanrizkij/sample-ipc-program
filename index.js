var net = require('net'),
    fs = require('fs'),
    connections = {},
    server;

// prevent duplicate exit messages
var SHUTDOWN = false;

// Our socket
const SOCKETFILE = '/tmp/sockets.sock';

console.info('Loading interprocess communications test');
console.info('  Unix sock Server: \n  Socket: %s \n  Process: %s',SOCKETFILE,process.pid);

function createServer(socket){
    console.log('Creating server.');
    var server = net.createServer(function(stream) {
        console.log('Connection acknowledged.');
        var self = Date.now();
        connections[self] = (stream);
        stream.on('end', function() {
            console.log('Client disconnected.');
            delete connections[self];
        });

        // Messages are buffers. use toString
        stream.on('data', function(msg) {
            msg = msg.toString();
            resp = configureResponse(msg)
            console.log("Result : ", resp)
            stream.write(JSON.stringify(resp))
        });
    })
    .listen(socket)
    .on('connection', function(socket){
        console.log('Client connected.');
    });
    return server;
}

let configureResponse = (data)=> {
    let json = data.split("\\n")
    let result = {}
    for(let item of json){
        let text = item.replace("\\","")
        if(text && text!='') {
	    text = text.toString()
	   try{
             json = JSON.parse(text)
	   
            }catch(e){
                continue;
		json = text
	    }
            let array=[];
            for(i=json.from;i<=json.to;i++) {
                if(i%3==0 && i%5==0) {
                    array.push(`${json.fizz}${json.buzz}`)
                }else if(i%3==0){
                    array.push(`${json.fizz}`)
                }else if(i%5==0){
                    array.push(`${json.buzz}`)
                }else {
                    array.push(i)
                }
            }
            result[json.id] = array
        }
    }
    return result
}

console.log('Checking for leftover socket.');
fs.stat(SOCKETFILE, function (err, stats) {
    if (err) {
        // start server
        console.log('No leftover socket found.');
        server = createServer(SOCKETFILE); return;
    }
    // remove file then start server
    console.log('Removing leftover socket.')
    fs.unlink(SOCKETFILE, function(err){
        if(err){
            // This should never happen.
            console.error(err); process.exit(0);
        }
        server = createServer(SOCKETFILE); return;
    }); 
//server  = createServer(SOCKETFILE);return; 
});

// close all connections when the user does CTRL-C
function cleanup(){
    if(!SHUTDOWN){ SHUTDOWN = true;
        console.log('\n',"Terminating.",'\n');
        if(Object.keys(connections).length){
            let clients = Object.keys(connections);
            while(clients.length){
                let client = clients.pop();
                connections[client].write('__disconnect');
                connections[client].end(); 
            }
        }
        server.close();
        process.exit(0);
    }
}
process.on('SIGINT', cleanup);

