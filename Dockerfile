FROM node:14-alpine3.14
COPY package*.json ./
WORKDIR /app
RUN npm install
COPY . .
ENTRYPOINT ["node","index.js"]
